import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { JobService } from '../job.service';
import { JobtypeService } from '../jobtype.service';
import { SensorService } from '../sensor.service';
import { Task } from "../task"
@Component({
  selector: 'app-addjob',
  templateUrl: './addjob.component.html',
  styleUrls: ['./addjob.component.css']
})
export class AddjobComponent implements OnInit {

  rows: Task[] = []
  job: Task = { id: 0, tl: 0, th: 0, hl: 0, hh: 0 }
  bag = { obj: { id: 0, name: '' } }
  jobtypebag = { obj: { id: 0, name: '' } }
  constructor(public ss: SensorService, public jts: JobtypeService, public js: JobService, private bar: MatSnackBar) { }
  update() {
    this.js.sn({ search: '', page: 0, limit: 1000 }).subscribe((d:any) => {
      this.rows = d
    })
  }
  enable(o:Task,index:number)
  {
    // o.enable = !o.enable
    this.js.edit(o).subscribe(d=>{
      console.log('Edit ',o)
      this.rows[index] = o
    })
  }
  ngOnInit(): void {
    this.update()
  }
  save() {

    this.job.jobtype = this.jobtypebag.obj
    this.job.sensor = this.bag.obj

    this.js.add(this.job).subscribe(d => {
      console.log('Add ', d)
      this.bar.open('Add', '', { duration: 2000 })
      this.job = {}
      this.update();
    })


  }

}
