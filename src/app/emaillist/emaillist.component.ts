import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Email } from '../email';
import { EmailService } from '../email.service';
import { SaveconfigService } from '../saveconfig.service';

@Component({
  selector: 'app-emaillist',
  templateUrl: './emaillist.component.html',
  styleUrls: ['./emaillist.component.css']
})
export class EmaillistComponent implements OnInit {
  email: any
  sdate: any
  edate: any
  rows: Email[] = []
  constructor(private es: EmailService, private bar: MatSnackBar, private ss: SaveconfigService) { }

  update() {
    this.es.sn(this.sdate, this.edate).subscribe((d) => {
      console.log('Data ', d)
      this.rows = d as Email[]
    })
  }
  enable(o:Email,index:number)
  {
    // o.enable = !o.enable
    this.es.edit(o).subscribe(d=>{
      console.log('Edit ',o)
      this.rows[index] = o
    })
  }
  addemail() {
    this.es.add(this.email).subscribe(d => {
      this.bar.open('Add', 'Email', { duration: 2000 })
      this.email = ""
      this.update()
    })
  }
  show() {
    this.ss.save('emaillist', { s: this.sdate, e: this.edate })
    this.es.sn(this.sdate, this.edate).subscribe(d => {
      this.rows = d as Email[]
    })
  }
  ngOnInit(): void {
    let o = this.ss.get('emaillist')
    if (o != null) {
      this.sdate = o.s
      this.edate = o.e
    }
    this.update()
  }

}
