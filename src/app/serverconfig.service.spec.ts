import { TestBed } from '@angular/core/testing';

import { ServerconfigService } from './serverconfig.service';

describe('ServerconfigService', () => {
  let service: ServerconfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServerconfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
