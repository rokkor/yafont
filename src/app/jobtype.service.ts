import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Searchobj } from '@kykub/base';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class JobtypeService {

  constructor(private http:HttpClient) { 


  }

  sn(s:Searchobj)
  {
    console.log('S object',s)
     let url = environment.serverurl+"/jobtype/sn"
      return this.http.post(url,{name:s.search,page:s.page,limit:s.limit})
  }
}
