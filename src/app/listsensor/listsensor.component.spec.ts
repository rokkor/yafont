import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListsensorComponent } from './listsensor.component';

describe('ListsensorComponent', () => {
  let component: ListsensorComponent;
  let fixture: ComponentFixture<ListsensorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListsensorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListsensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
