import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { SaveconfigService } from '../saveconfig.service';
import { Sensor } from '../sensor';
import { SensorService } from '../sensor.service';
import { SensorvalueService } from '../sensorvalue.service';

@Component({
  selector: 'app-listsensor',
  templateUrl: './listsensor.component.html',
  styleUrls: ['./listsensor.component.css']
})
export class ListsensorComponent implements OnInit {
  rows = Array<Sensor>()
  search = ""
  sdate: any
  edate: any
  constructor(private ss: SensorService,private svs:SensorvalueService, private bar: MatSnackBar,private sds:SaveconfigService) { }
  exporttoexcel() {
    this.svs.exportsensorvalues( this.sdate, this.edate).subscribe(d => {
      let s = moment(this.sdate)
      let e = moment(this.edate)
      let n = s.format("YYYY-MM-DD") + '_' + e.format("YYYY-MM-DD")
      FileSaver.saveAs(d, n + "-exports.xlsx");
      this.bar.open('Export ', 'Ok', { duration: 2000 })
    }, e => {
      this.bar.open("Export error", e, { duration: 5000 })
    })
  }
  searchbyname() {
    this.sds.save('listsensor',{s:this.sdate,e:this.edate,search:this.search})
    this.ss.searchsensor(this.search).subscribe((d: any) => {
      this.rows = d
    }, e => {
      this.bar.open('Not found', e, { duration: 2000 })
    })
  }
  ngOnInit(): void {
    let o = this.sds.get('listsensor')
    if(o!=null)
    {
      this.sdate = o.s
      this.edate = o.e
      this.search = o.search
    }
    this.ss.getSensor().subscribe((d: any) => {
      console.log('Sensor', d)
      this.rows = d
    })
  }

}
