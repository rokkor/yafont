import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { interval, Subscriber, Subscription } from 'rxjs';
import { SensorService } from '../sensor.service';
import { Sensorvalue } from '../sensorvalue';
import { SensorvalueService } from '../sensorvalue.service';

@Component({
  selector: 'app-realtime',
  templateUrl: './realtime.component.html',
  styleUrls: ['./realtime.component.css']
})
export class RealtimeComponent implements OnInit, OnDestroy {
  subscription: any
  timetoupdate = 5000
  page = 0
  limit = 10
  constructor(private svs: SensorvalueService, private bar: MatSnackBar) { }
  ngOnDestroy(): void {
    if (this.subscription)
      this.subscription.unsubscribe()
  }
  rows: Sensorvalue[] = []
  update() {
    this.svs.getlastValue(this.page, this.limit).subscribe((d: any) => {
      console.log('Data', d)
      this.rows = d
      this.bar.open('Update', '', { duration: 1000 })
    })
  }
  settime() {
    if (this.subscription) this.subscription.unsubscribe();
    this.subscription = interval(this.timetoupdate).subscribe((val) => {
      this.update()
    });
  }
  ngOnInit(): void {

    this.subscription = interval(this.timetoupdate).subscribe((val) => {
      this.update();
    });
    this.update();


  }

}
