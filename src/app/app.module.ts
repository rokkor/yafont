import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListsensorComponent } from './listsensor/listsensor.component';
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatMenuModule } from "@angular/material/menu";
import { RealtimeComponent } from './realtime/realtime.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SensorService } from './sensor.service';
import { SensorvalueService } from './sensorvalue.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AddserverconfigComponent } from './addserverconfig/addserverconfig.component';
import { ServerconfigService } from './serverconfig.service';
import { SensordetailComponent } from './sensordetail/sensordetail.component';
import { NgChartsModule } from 'ng2-charts';
import {
  MatDatepickerModule,
  MatMomentDateModule,
  MatNativeDateModule,
} from "@coachcare/datepicker";
import { SaveconfigService } from './saveconfig.service';
import { EmailService } from './email.service';
import { EmaillistComponent } from './emaillist/emaillist.component';
import { EditsensorComponent } from './editsensor/editsensor.component';
import { Kylist6Module } from '@kykub/list';
import { AutoModule } from '@kykub/auto';
import { SensorgroupService } from './sensorgroup.service';
import { MatCheckbox, MatCheckboxModule } from '@angular/material/checkbox';
import { AddjobComponent } from './addjob/addjob.component';
import { JobService } from './job.service';
import { ServerconfigeditComponent } from './serverconfigedit/serverconfigedit.component';
@NgModule({
  declarations: [
    AppComponent,
    ListsensorComponent,
    RealtimeComponent,
    AddserverconfigComponent,
    SensordetailComponent,
    EmaillistComponent,
    EditsensorComponent,
    AddjobComponent,
    ServerconfigeditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, MatFormFieldModule, MatInputModule, FormsModule,
    BrowserAnimationsModule,
    MatIconModule, MatButtonModule, MatMenuModule, MatSnackBarModule,
    MatDatepickerModule, MatMomentDateModule,MatCheckboxModule,
    HttpClientModule,
    NgChartsModule,
    Kylist6Module,AutoModule
  ],
  providers: [SensorService, SensorvalueService, ServerconfigService, SaveconfigService,EmailService,SensorgroupService,JobService],
  bootstrap: [AppComponent]
})
export class AppModule { }
