import { Jobtype } from "./jobtype"
import { Sensor } from "./sensor"

export interface Task {
    id?: number | null | undefined
    name?: number | null | undefined
    jobtype?: Jobtype | null | undefined
    sensor?: Sensor | null | undefined
    tl?: number | null | undefined
    th?: number | null | undefined
    hl?: number | null | undefined
    hh?: number | null | undefined
    enable?: boolean | null | undefined
}
