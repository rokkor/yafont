import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SensordetailComponent } from './sensordetail.component';

describe('SensordetailComponent', () => {
  let component: SensordetailComponent;
  let fixture: ComponentFixture<SensordetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SensordetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SensordetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
