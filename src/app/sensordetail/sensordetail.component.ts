import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Route } from '@angular/router';
import { Sensor } from '../sensor';
import { SensorService } from '../sensor.service';
import { SensorvalueService } from '../sensorvalue.service';
import { ChartConfiguration, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import { default as Annotation } from 'chartjs-plugin-annotation';
import { Sensorvalue } from '../sensorvalue';
import { SaveconfigService } from '../saveconfig.service';
import * as moment from 'moment';
import * as FileSaver from 'file-saver';
import { saveAs } from 'file-saver';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-sensordetail',
  templateUrl: './sensordetail.component.html',
  styleUrls: ['./sensordetail.component.css']
})
export class SensordetailComponent implements OnInit {
  sdate: any
  edate: any
  sensor: Sensor = { id: 0 }
  lables: string[] = []
  public lineChartData: ChartConfiguration['data'] = {
    datasets: [
      {
        data: [65, 59, 80, 81, 56, 55, 40],
        label: 'T',
        backgroundColor: 'rgba(148,159,177,0.2)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)',
        fill: 'origin',
      },
      {
        data: [28, 48, 40, 19, 86, 27, 90],
        label: 'H',
        backgroundColor: 'rgba(77,83,96,0.2)',
        borderColor: 'rgba(77,83,96,1)',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)',
        fill: 'origin',
      },
    
    ],
    labels: this.lables
  };

  public lineChartOptions: ChartConfiguration['options'] = {
    elements: {
      line: {
        tension: 0.5
      }
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      x: {},
      'y-axis-0':
      {
        position: 'left',
      },
      'y-axis-1': {
        position: 'right',
        grid: {
          color: 'rgba(255,0,0,0.3)',
        },
        ticks: {
          color: 'red'
        }
      }
    },

    plugins: {
      legend: { display: true },
      annotation: {
        annotations: [
          {
            type: 'line',
            scaleID: 'x',
            value: 'March',
            borderColor: 'orange',
            borderWidth: 2,
            label: {
              position: 'center',
              enabled: true,
              color: 'orange',
              content: 'LineAnno',
              font: {
                weight: 'bold'
              }
            }
          },
        ],
      }
    }
  };

  public lineChartType: ChartType = 'line';

  @ViewChild(BaseChartDirective) chart?: BaseChartDirective;


  constructor(private route: ActivatedRoute, private ss: SensorService, private svs: SensorvalueService,
    private saves: SaveconfigService, private bar: MatSnackBar) {



    route.params.subscribe(p => {
      console.log("id", p["id"])

      this.ss.get(p["id"]).subscribe(d => {
        console.log('Found', d)
        this.sensor = d
      })
    })
  }
  exporttoexcel() {
    this.svs.exportsensorvalue(this.sensor.id, this.sdate, this.edate).subscribe(d => {
      let s = moment(this.sdate)
      let e = moment(this.edate)
      let n = s.format("YYYY-MM-DD") + '_' + e.format("YYYY-MM-DD")
      FileSaver.saveAs(d, n + "-exports.xlsx");
      this.bar.open('Export ', 'Ok', { duration: 2000 })
    }, e => {
      this.bar.open("Export error", e, { duration: 5000 })
    })
  }
  show() {
    console.log('Sdate', this.sdate)
    console.log("edate", this.edate)
    this.saves.save('sensordetail', { s: this.sdate, e: this.edate })
    this.svs.getBydate(this.sensor.id, this.sdate, this.edate).subscribe((d: any) => {
      console.log('Result', d)
      this.lineChartData.labels = []
      this.lineChartData.datasets[0].data = []
      this.lineChartData.datasets[1].data = []
      console.log('Line', this.lineChartData)
      // d.forEach(this.updatedata);
      d.map((i: Sensorvalue) => {
        let time = i.adddate?.split('T')[1].split('.')[0]
        this.lineChartData.labels?.push(time)
        let t = i.t as number
        let h = i.h as number
        this.lineChartData.datasets[0].data.push(t)
        this.lineChartData.datasets[1].data.push(h)
      })

      this.chart?.update();
      this.bar.open('Get data', '', { duration: 2000 })
    })
  }
  ngOnInit(): void {
    let o: any = this.saves.get('sensordetail')
    console.log('Save obj', o)
    if (o != null) {
      this.sdate = o.s
      this.edate = o.e
    }
  }

}
