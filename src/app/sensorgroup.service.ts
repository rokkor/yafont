import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Searchobj } from '@kykub/base';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SensorgroupService {

  constructor(private http: HttpClient) {


  }
  sn(s: Searchobj) {
    let url = environment.serverurl + "/sensorgroup/sn"
    return this.http.post(url, { name: s.search, page: s.page, limit: s.limit })
  }
}
