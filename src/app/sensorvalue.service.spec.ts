import { TestBed } from '@angular/core/testing';

import { SensorvalueService } from './sensorvalue.service';

describe('SensorvalueService', () => {
  let service: SensorvalueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SensorvalueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
