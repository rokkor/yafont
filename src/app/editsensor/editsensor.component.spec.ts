import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditsensorComponent } from './editsensor.component';

describe('EditsensorComponent', () => {
  let component: EditsensorComponent;
  let fixture: ComponentFixture<EditsensorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditsensorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditsensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
