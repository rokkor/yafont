import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { Sensor } from '../sensor';
import { SensorService } from '../sensor.service';
import { SensorgroupService } from '../sensorgroup.service';
@Component({
  selector: 'app-editsensor',
  templateUrl: './editsensor.component.html',
  styleUrls: ['./editsensor.component.css']
})
export class EditsensorComponent implements OnInit {
  sensor: Sensor = {}
  sensorgroupbag = { obj: { id: 0, name: '' } }
  constructor(private router: ActivatedRoute, public ss: SensorService, public sgs: SensorgroupService, private bar: MatSnackBar) { }

  ngOnInit(): void {
    this.router.params.subscribe(d => {
      console.log('Paramer', d)
      this.ss.get(d["id"]).subscribe(sensor => {
        console.log('Sensor', sensor)
        this.sensor = sensor
      })
    })

  }

  save() {

    console.log('Save data', this.sensor)
    this.sensor.sensorgroup = this.sensorgroupbag.obj
    this.ss.edit(this.sensor).subscribe(d => {
      this.bar.open('Save', '', { duration: 2000 })
    })
  }
}
