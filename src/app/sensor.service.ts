import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../environments/environment';
import { Sensor } from './sensor';
@Injectable({
  providedIn: 'root'
})
export class SensorService {

  constructor(private http: HttpClient) { }


  getSensor(page = 0, limit = 1000) {
    return this.http.get(environment.serverurl + "/sensorlist")
  }
  searchsensor(n = "") {
    return this.http.get(environment.serverurl + "/searchsensor/" + n)
  }
  get<Sensor>(id: number) {
    return this.http.get(environment.serverurl + "/getsensor/" + id)
  }
  sn(sobj = "", page = 0, limit = 100) {
    let s = { id: 0, name: sobj.search }
    console.log('for search', s)

    return this.http.post(environment.serverurl + "/sensor/sn",s)
  }
  edit(sensor: Sensor) {
    return this.http.post(environment.serverurl + "/sensoredit", sensor)
  }
}
