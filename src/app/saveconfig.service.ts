import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SaveconfigService {

  constructor() { }

  save(name: string, obj: any) {
    localStorage.setItem(name, JSON.stringify(obj))
  }
  get(name: string) {
    let o = localStorage.getItem(name)
    if (o != null)
      return JSON.parse(o)
    return null
  }
 
}
