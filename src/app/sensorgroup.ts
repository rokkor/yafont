export interface Sensorgroup {
    id?: number | null | undefined
    name?: string | null | undefined
    description?: string | null | undefined
}
