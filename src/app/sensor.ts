import { Sensorgroup } from "./sensorgroup";

export interface Sensor {
    sensorid?: String | null;
    name?: String | null;
    id?: number | null;
    sensorgroup_id?: number | null;
    description?: string | null
    sensorgroup?: Sensorgroup | null | undefined
}
