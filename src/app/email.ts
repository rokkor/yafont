export interface Email {
    id?: number | null
    name?: string | null
    enable?: boolean | null
    ver?: number | null
}
