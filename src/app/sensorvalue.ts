export interface Sensorvalue {
    id?:number;
    sensorid?:String;
    t?:number;
    h?:number;
    valuedate?:Date;
    adddate?:string;
}
