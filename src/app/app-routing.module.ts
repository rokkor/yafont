import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddjobComponent } from './addjob/addjob.component';
import { AddserverconfigComponent } from './addserverconfig/addserverconfig.component';
import { EditsensorComponent } from './editsensor/editsensor.component';
import { EmaillistComponent } from './emaillist/emaillist.component';
import { ListsensorComponent } from './listsensor/listsensor.component';
import { RealtimeComponent } from './realtime/realtime.component';
import { SensordetailComponent } from './sensordetail/sensordetail.component';
import { ServerconfigeditComponent } from './serverconfigedit/serverconfigedit.component';

const routes: Routes = [
  { path: 'sensor', component: ListsensorComponent },
  { path: 'realtime', component: RealtimeComponent },
  { path: 'addconfig', component: AddserverconfigComponent },
  { path: 'sensordetail/:id', component: SensordetailComponent },
  { path: 'email', component: EmaillistComponent },
  { path: 'editsensor/:id', component: EditsensorComponent },
  { path: 'addjob', component: AddjobComponent },
  { path: 'serverconfigedit/:id', component: ServerconfigeditComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false, useHash: true, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
