import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Email } from './email';
@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(private http: HttpClient) {

  }
  edit(e:Email)
  {
    return this.http.post(environment.serverurl+"/email/edit",e)
  }
  add(e: any) {
    return this.http.get(environment.serverurl + "/addemail/" + e)
  }
  sn(s: any, e: any) {
    return this.http.get(environment.serverurl + "/emaillist")
  }
}
