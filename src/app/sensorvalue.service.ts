import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SensorvalueService {

  constructor(private http: HttpClient) { }
  getlastValue(page = 0, limit = 100) {
    let url = environment.serverurl + '/lastvalues/' + page + '/' + limit
    return this.http.get(url)
  }
  getBydate(id: any, s: any, e: any) {
    let url = environment.serverurl + '/getsensorvalue'
    return this.http.post(url, { id: id, sdate: s, edate: e })
  }
  exportsensorvalues(s: any, e: any) {
    let url = environment.serverurl + '/exportsensorvalues'
    return this.http.post(url, { sdate: s, edate: e }, { responseType: "blob" })
  }
  exportsensorvalue(id?: number | null, s?: any, e?: any) {
    let url = environment.serverurl + '/exportsensorvalue'
    return this.http.post(url, { id: id, sdate: s, edate: e }, { responseType: "blob" })
  }
}
