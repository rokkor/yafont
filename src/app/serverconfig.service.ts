import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Serverconfig } from './serverconfig';
@Injectable({
  providedIn: 'root'
})
export class ServerconfigService {

  constructor(private http:HttpClient) { }

  setconfig(configname:string,value:string)
  {
    let url = environment.serverurl+"/setconfig/"+configname+"/"+value
    return this.http.get(url)
  }

  getconfig()
  {
    let url = environment.serverurl+"/configlist"
    return  this.http.get(url)
  }
  delete(id:any)
  {
    let url = environment.serverurl+'/config/delete/'+id
    return this.http.get(url)
  }
  get(id:any)
  {
    let url = environment.serverurl+"/config/get/"+id
    return this.http.get(url)
  }
  edit(cfg:Serverconfig)
  {
    let url = environment.serverurl+"/config/edit"
    return this.http.post(url,cfg)
  }

}
