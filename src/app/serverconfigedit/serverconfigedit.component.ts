import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { Serverconfig } from '../serverconfig';
import { ServerconfigService } from '../serverconfig.service';

@Component({
  selector: 'app-serverconfigedit',
  templateUrl: './serverconfigedit.component.html',
  styleUrls: ['./serverconfigedit.component.css']
})
export class ServerconfigeditComponent implements OnInit {

  config: Serverconfig = {}
  constructor(private router: ActivatedRoute, private cfs: ServerconfigService, private bar: MatSnackBar) { }

  ngOnInit(): void {
    this.router.params.subscribe(d => {
      this.cfs.get(d["id"]).subscribe(cfg => {
        this.config = cfg
      })
    })
  }

  edit() {
    this.cfs.edit(this.config).subscribe(d => {
      this.config = d
      this.bar.open('Edit ', '', { duration: 2000 })
    })
  }

}
