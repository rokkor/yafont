import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerconfigeditComponent } from './serverconfigedit.component';

describe('ServerconfigeditComponent', () => {
  let component: ServerconfigeditComponent;
  let fixture: ComponentFixture<ServerconfigeditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServerconfigeditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ServerconfigeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
