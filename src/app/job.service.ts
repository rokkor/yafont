import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Task } from './task'
@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(private http: HttpClient) { }

  add(job: Task | null | undefined) {
    let url = environment.serverurl + '/job/add'
    return this.http.post(url, job)
  }
  sn(s:any)
  {
    let url = environment.serverurl+'/job/sn'
    return this.http.post(url,s)
  }
  edit(t:Task)
  {
    let url = environment.serverurl+'/job/edit'
    return this.http.post(url,t)
  }
}
