import { TestBed } from '@angular/core/testing';

import { SensorgroupService } from './sensorgroup.service';

describe('SensorgroupService', () => {
  let service: SensorgroupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SensorgroupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
