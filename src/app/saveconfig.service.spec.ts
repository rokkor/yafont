import { TestBed } from '@angular/core/testing';

import { SaveconfigService } from './saveconfig.service';

describe('SaveconfigService', () => {
  let service: SaveconfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaveconfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
