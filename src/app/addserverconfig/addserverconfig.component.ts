import { Component, OnInit } from '@angular/core';
import { Serverconfig } from '../serverconfig';
import { ServerconfigService } from '../serverconfig.service';

@Component({
  selector: 'app-addserverconfig',
  templateUrl: './addserverconfig.component.html',
  styleUrls: ['./addserverconfig.component.css']
})
export class AddserverconfigComponent implements OnInit {

  constructor(private sc:ServerconfigService) { }
  rows:Serverconfig[] = []
  configname = ""
  configvalue = ""
  save()
  {
      this.sc.setconfig(this.configname,this.configvalue).subscribe(d=>{
        this.configname=""
        this.configvalue=""
        this.update()
      })
  }
  update()
  {
    this.sc.getconfig().subscribe(d=>{
      this.rows = d as any
      console.log('Found config',d)

    })
  }
  delete(id:any,i:any)
  {
    this.sc.delete(id).subscribe(d=>{
      this.rows.splice(i,1)
    })
  }
  ngOnInit(): void {
    this.update()
  }

}
